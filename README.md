# What the hell is this?

This is a plugin I wrote with my friends that hits the worlds greatest comic book API (https://developer.marvel.com/).

You can search your favorites heroes, see their pictures, descriptions etc. Written in Javascript.