define(['marvel/api'], function(marvelApi) {
	describe('getCharacters', function() {
		it('queries the marvel api', function(done) {
			marvelApi.getCharacters()
			.done(function(data) {
				expect(data.data).toBeDefined();
			})
			.fail(function() {
				fail('ajax called failed');
			})
			.always(done);
		});
		
		it('finds the right character', function(done) {
			marvelApi.getCharacters('Spider-')
			.done(function(data) {
				expect(data.data.results[0].name).toMatch(/Spider-.*/);
			})
			.fail(function() {
				fail('ajax called failed');
			})
			.always(done);
		})
	});


	describe('getComics', function() {
		it('queries the marvel api', function(done) {
			marvelApi.getComics()
			.done(function(data) {
				expect(data.data).toBeDefined();
			})
			.fail(function() {
				fail('ajax called failed');
			})
			.always(done);
		});
		
		it('finds the right comic book', function(done) {
			marvelApi.getComics('Armor')
			.done(function(data) {
				expect(data.data.results[0].title).toMatch(/Armor.*/);
			})
			.fail(function() {
				fail('ajax called failed');
			})
			.always(done);
		})
	});


	describe('getEvents', function() {
		it('queries the marvel api', function(done) {
			marvelApi.getEvents()
			.done(function(data) {
				expect(data.data).toBeDefined();
			})
			.fail(function() {
				fail('ajax called failed');
			})
			.always(done);
		});
		
		it('finds the right comic book', function(done) {
			marvelApi.getEvents('The')
			.done(function(data) {
				expect(data.data.results[0].title).toMatch(/The.*/);
			})
			.fail(function() {
				fail('ajax called failed');
			})
			.always(done);
		})
	});
});
