var tests = [];
for ( var file in window.__karma__.files) {
	if (/Spec\.js$/.test(file)) {
		tests.push(file);
	}
}

requirejs.config({
	// Karma serves files from '/base'
	baseUrl: '/base/src',
	config: {
		'marvel/api': {
			API_BASE_ENDPOINT: 'https://gateway.marvel.com/',
			PRIVATE_KEY: "3ad8a736ed380d5ec9ad23ddc0a4725df2f99cb8",
			PUBLIC_KEY: "2b877f1d164c6ce96e34bd1bd1c7ca17",
		}
	},

	paths: {
		jquery: '/base/lib/jquery',
		'jquery_md5': '/base/lib/jquery_md5',
		domReady: '/base/lib/domReady'
	},

	shim: {
		'jquery_md5': {
			'deps': [
				'jquery'
			],
			'exports': 'jQuery.fn.md5'
		}
	},

	// ask Require.js to load these files (all our tests)
	deps: tests,

	// start test run, once Require.js is done
	callback: window.__karma__.start
});
