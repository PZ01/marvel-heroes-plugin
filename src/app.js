requirejs.config({
	config: {
		'marvel/api': {
			API_BASE_ENDPOINT: 'https://gateway.marvel.com/',
			PRIVATE_KEY: "3ad8a736ed380d5ec9ad23ddc0a4725df2f99cb8",
			PUBLIC_KEY: "2b877f1d164c6ce96e34bd1bd1c7ca17"
		}
	},
	baseUrl: 'src',
	shim: {
		'jquery_md5': {
			'deps': [
				'jquery'
			],
			'exports': 'jQuery.fn.md5'
		}
	},
	paths: {
		domReady: '../lib/domReady',
		jquery: '../lib/jquery',
		jquery_md5: '../lib/jquery_md5',
		mustache: '../lib/mustache'
	}
});

requirejs([
	"marvelwidget", "domReady!"
], function(marvelwidget, domReady) {
	new marvelwidget("#marvelWidget");
});
