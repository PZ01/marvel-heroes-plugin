define([
	"module", "jquery", "marvel/api", "mustache",
], function(module, $, marvelApi, mustache) {
	'use strict';
	var ENTER_KEYCODE = 13;

	var Widget = function(containerSelector) {
		var self = this;
		self.$container = $(containerSelector);
		self.$searchButton = undefined;
		self.$searchField = undefined;
		self.$characterResults = undefined;
		self.$characterSearchSpinner = undefined;
		self.$comicResults = undefined;
		self.$comicSearchSpinner = undefined;
		self.$eventResults = undefined;
		self.$eventSearchSpinner = undefined;
		self.$infoContainer = undefined;
		self.$resultsContainer = undefined;
		self.$detailsContainer = undefined;
		self.currentCharacterSearchXhr = undefined;
		self.currentComicSearchXhr = undefined;
		self.currentEventSearchXhr = undefined;

		$.ajax({
			url: '/templates/widget.mst',
			dataType: 'text',
			cache: false,
		}).done(function(template) {
			self.renderWidget(template);
			self.setUiCallbacks();
		})

		self.renderWidget = function(template) {
			self.$container.append(mustache.render(template));
			self.$searchButton = self.$container.find("button.search");
			self.$searchField = self.$container.find('input.search');
			self.$eventResults = self.$container.find('ul.events');
			self.$eventSearchSpinner = self.$container.find('div.event-search-spinner');
			self.$characterResults = self.$container.find('ul.characters');
			self.$characterSearchSpinner = self.$container.find('div.character-search-spinner');
			self.$comicResults = self.$container.find('ul.comics');
			self.$comicSearchSpinner = self.$container.find('div.comic-search-spinner');
			self.$infoContainer = self.$container.find('div.info');
			self.$resultsContainer = self.$container.find("div.results");
			self.$detailsContainer = self.$container.find("div.details");

			self.$resultsContainer.hide();
		}

		self.setUiCallbacks = function() {
			self.$searchButton.click(self.search);
			self.$searchField.keyup(function(event) {
				if (event.keyCode == ENTER_KEYCODE) {
					self.search();
				}
			});
			self.$characterSearchSpinner.find('a.cancel-search').click(self.abortCharacterSearch);
			self.$comicSearchSpinner.find('a.cancel-search').click(self.abortComicSearch);
			self.$eventSearchSpinner.find('a.cancel-search').click(self.abortEventSearch);
		}

		self.search = function() {
			self.$infoContainer.hide();
			self.$characterResults.hide();
			self.$comicResults.hide();
			self.$eventResults.hide();

			self.$detailsContainer.empty();

			self.$characterSearchSpinner.show();
			self.$comicSearchSpinner.show();
			self.$eventSearchSpinner.show();

			self.$resultsContainer.show(); // result list may be hidden when showing details

			self.currentCharacterSearchXhr = marvelApi.getCharacters(self.$searchField.val()).done(self.showCharacterResults).error(self.onCharacterQueryFailure);
			self.currentComicSearchXhr = marvelApi.getComics(self.$searchField.val()).done(self.showComicResults).error(self.onEventQueryFailure);
			self.currentEventSearchXhr = marvelApi.getEvents(self.$searchField.val()).done(self.showEventResults).error(self.onComicQueryFailure);
		}

		self.showCharacterResults = function(json) {
			self.$characterSearchSpinner.hide();
			self.$characterResults.empty();
			var characterItemTemplate = '<li><a class="resultlink" href="javascript:void(0)">{{ name }}</a></li>'
			for (var i=0; i<Math.min(json.data.count, 3); ++i) {
				var $characterItem = $(mustache.render(characterItemTemplate, json.data.results[i]));
				self.$characterResults.append($characterItem);
				self.setCharacterResultUiCallbacks($characterItem, json.data.results[i]);
			}
			self.$characterResults.show();
		}

		self.abortCharacterSearch = function() {
			if (undefined != self.currentCharacterSearchXhr) {
				self.currentCharacterSearchXhr.abort();
				self.$characterResults.show();
				self.$characterSearchSpinner.hide();
			}
		};

		self.onCharacterQueryFailure = function(jqxhr, textStatus, errorThrown) {
			self.$characterSearchSpinner.hide();
			var errorMessageTemplate = '<div class="error"> Oops, something went wrong !</div>';
			if (textStatus != "abort") {
				self.$characterResults.append($(errorMessageTemplate));
			}
		}

		self.setCharacterResultUiCallbacks = function ( $characterItem, jsonResults ) {
			$characterItem.find('a.resultlink').click( function () {
				self.$resultsContainer.hide();
				$.ajax({
					url: '/templates/character.mst',
					dataType: 'text',
					cache: false,
				}).done(function(template) {
					self.renderCharacter(template, jsonResults);
				})
			});
		}

		self.renderCharacter = function ( template, jsonResults ) {
			self.$detailsContainer.empty();
			self.$detailsContainer.append(mustache.render(template, jsonResults));
			if ( jsonResults.comics.items.length > 0 ) {
				var $relatedComics = $("<div> <h4> Related Comics: </h4> <ul class='relatedComicList'> </ul></div>");
				var $relatedComicsContainer = $relatedComics.find(".relatedComicList");
				self.$detailsContainer.append($relatedComics);
				for (var i=0; i<Math.min(jsonResults.comics.items.length, 3); ++i) {
					self.showRelatedComic($relatedComicsContainer, jsonResults.comics.items[i]);
				}
			}
			if ( jsonResults.events.items.length > 0 ) {
				var $relatedEvents = $("<div> <h4> Related Events: </h4> <ul class='relatedEventList'> </ul></div>");
				var $relatedEventsContainer = $relatedEvents.find(".relatedEventList");
				self.$detailsContainer.append($relatedEvents);
				for (var i=0; i<Math.min(jsonResults.events.items.length, 3); ++i) {
					self.showRelatedEvent($relatedEventsContainer, jsonResults.events.items[i]);
				}
			}
		}

		self.showRelatedCharacter = function ( $container, json ) {
			var relatedCharacterTemplate = '<li><a class="relatedcharacter" href="javascript:void(0)">{{ name }}</a></li>';
			var $relatedCharacter = $(mustache.render(relatedCharacterTemplate, json));
			$container.append($relatedCharacter);
			self.setRelatedCharacterUICallbacks($relatedCharacter, json);
		}

		self.setRelatedCharacterUICallbacks = function ( $relatedCharacter, json ) {
			$relatedCharacter.find("a").click( function () {
				self.$detailsContainer.empty();
				var spinnerImageTag = '<img class="spinnerImage" src="res/spinner.gif"/>';
				self.$detailsContainer.append(spinnerImageTag);
				marvelApi.query(json.resourceURI, {}).done( function ( jsonResults ) {
					$.ajax({
						url: '/templates/character.mst',
						dataType: 'text',
						cache: false,
					}).done(function(template) {
						self.renderCharacter(template, jsonResults.data.results[0]);
					}).error(self.showErrorMessageInDetails);
				}).error(self.showErrorMessageInDetails);
			});
		}

		self.showRelatedComic = function ( $container, json ) {
			var relatedComicTemplate = '<li><a class="relatedcomic" href="javascript:void(0)">{{ name }}</a></li>';
			var $relatedComic = $(mustache.render(relatedComicTemplate, json));
			$container.append($relatedComic);
			self.setRelatedComicUICallbacks($relatedComic, json);
		}

		self.setRelatedComicUICallbacks = function ( $relatedComic, json ) {
			$relatedComic.find("a").click( function () {
				self.$detailsContainer.empty();
				var spinnerImageTag = '<img class="spinnerImage" src="res/spinner.gif"/>';
				self.$detailsContainer.append(spinnerImageTag);
				marvelApi.query(json.resourceURI, {}).done( function ( jsonResults ) {
					$.ajax({
						url: '/templates/comic.mst',
						dataType: 'text',
						cache: false,
					}).done(function(template) {
						self.renderComic(template, jsonResults.data.results[0]);
					}).error(self.showErrorMessageInDetails);
				}).error(self.showErrorMessageInDetails);
			});
		}

		self.showEventResults = function(json) {
			self.$eventSearchSpinner.hide();
			self.$eventResults.empty();
			var eventItemTemplate = '<li><a class="resultlink" href="javascript:void(0)">{{ title }}</a></li>'
			for (var i=0; i<Math.min(json.data.count, 3); ++i) {
				var $eventItem = $(mustache.render(eventItemTemplate, json.data.results[i]));
				self.$eventResults.append($eventItem);
				self.setEventResultUiCallbacks($eventItem, json.data.results[i]);
			}
			self.$eventResults.show();
		}

		self.abortEventSearch = function() {
			if (undefined != self.currentEventSearchXhr) {
				self.currentEventSearchXhr.abort();
				self.$eventResults.show();
				self.$eventSearchSpinner.hide();
			}
		};

		self.onEventQueryFailure = function(jqxhr, textStatus, errorThrown) {
			self.$eventSearchSpinner.hide();
			var errorMessageTemplate = '<div class="error"> Oops, something went wrong !</div>';
			if (textStatus != "abort") {
				self.$eventResults.append($(errorMessageTemplate));
			}
		}

		self.setEventResultUiCallbacks = function ( $eventItem, jsonResults ) {
			$eventItem.find('a.resultlink').click( function () {
				self.$resultsContainer.hide();
				$.ajax({
					url: '/templates/event.mst',
					dataType: 'text',
					cache: false,
				}).done(function(template) {
					self.renderEvent(template, jsonResults);
				})
			});
		}

		self.renderEvent = function ( template, jsonResults ) {
			self.$detailsContainer.empty();
			self.$detailsContainer.append(mustache.render(template, jsonResults));
			if ( jsonResults.characters.items.length > 0 ) {
				var $relatedCharacters = $("<div> <h4> Related Characters: </h4> <ul class='relatedCharacterList'> </ul></div>");
				var $relatedCharacterContainer = $relatedCharacters.find(".relatedCharacterList");
				self.$detailsContainer.append($relatedCharacters);
				for (var i=0; i<Math.min(jsonResults.characters.items.length, 3); ++i) {
					self.showRelatedCharacter($relatedCharacterContainer, jsonResults.characters.items[i]);
				}
			}
			if ( jsonResults.comics.items.length > 0 ) {
				var $relatedComics = $("<div> <h4> Related Comics: </h4> <ul class='relatedComicList'> </ul></div>");
				var $relatedComicsContainer = $relatedComics.find(".relatedComicList");
				self.$detailsContainer.append($relatedComics);
				for (var i=0; i<Math.min(jsonResults.comics.items.length, 3); ++i) {
					self.showRelatedComic($relatedComicsContainer, jsonResults.comics.items[i]);
				}
			}
		}

		self.showRelatedEvent = function ( $container, json ) {
			var relatedEventTemplate = '<li><a class="relatedevent" href="javascript:void(0)">{{ name }}</a></li>';
			var $relatedEvent = $(mustache.render(relatedEventTemplate, json));
			$container.append($relatedEvent);
			self.setRelatedEventUICallbacks($relatedEvent, json);
		}

		self.setRelatedEventUICallbacks = function ( $relatedEvent, json ) {
			$relatedEvent.find("a").click( function () {
				self.$detailsContainer.empty();
				var spinnerImageTag = '<img class="spinnerImage" src="res/spinner.gif"/>';
				self.$detailsContainer.append(spinnerImageTag);
				marvelApi.query(json.resourceURI, {}).done( function ( jsonResults ) {
					$.ajax({
						url: '/templates/event.mst',
						dataType: 'text',
						cache: false,
					}).done(function(template) {
						self.renderEvent(template, jsonResults.data.results[0]);
					}).error(self.showErrorMessageInDetails);
				}).error(self.showErrorMessageInDetails);
			});
		}

		self.showComicResults = function(json) {
			self.$comicSearchSpinner.hide()
			self.$comicResults.empty();
			var comicItemTemplate = '<li><a class="resultlink" href="javascript:void(0)">{{ title }}</a></li>'
			for (var i=0; i<Math.min(json.data.count, 3); ++i) {
				var $comicItem = $(mustache.render(comicItemTemplate, json.data.results[i]));
				self.$comicResults.append($comicItem);
				self.setComicResultUiCallbacks($comicItem, json.data.results[i]);
			}
			self.$comicResults.show();
		}

		self.abortComicSearch = function() {
			if (undefined != self.currentComicSearchXhr) {
				self.currentComicSearchXhr.abort();
				self.$comicResults.show();
				self.$comicSearchSpinner.hide();
			}
		};

		self.onComicQueryFailure = function(jqxhr, textStatus, errorThrown) {
			self.$comicSearchSpinner.hide();
			var errorMessageTemplate = '<div class="error"> Oops, something went wrong !</div>';
			if (textStatus != "abort") {
				self.$comicResults.append($(errorMessageTemplate));
			}
		}

		self.setComicResultUiCallbacks = function ( $comicItem, jsonResults ) {
			$comicItem.find('a.resultlink').click( function () {
				self.$resultsContainer.hide();
				$.ajax({
					url: '/templates/comic.mst',
					dataType: 'text',
					cache: false,
				}).done(function(template) {
					self.renderComic(template, jsonResults);
				})
			});
		}

		self.renderComic = function ( template, jsonResults ) {
			self.$detailsContainer.empty();
			self.$detailsContainer.append(mustache.render(template, jsonResults));
			if ( jsonResults.characters.items.length > 0 ) {
				var $relatedCharacters = $("<div> <h4> Related Characters: </h4> <ul class='relatedCharacterList'> </ul></div>");
				var $relatedCharacterContainer = $relatedCharacters.find(".relatedCharacterList");
				self.$detailsContainer.append($relatedCharacters);
				for (var i=0; i<Math.min(jsonResults.characters.items.length, 3); ++i) {
					self.showRelatedCharacter($relatedCharacterContainer, jsonResults.characters.items[i]);
				}
			}
			if ( jsonResults.events.items.length > 0 ) {
				var $relatedEvents = $("<div> <h4> Related Events: </h4> <ul class='relatedEventList'> </ul></div>");
				var $relatedEventsContainer = $relatedEvents.find(".relatedEventList");
				self.$detailsContainer.append($relatedEvents);
				for (var i=0; i<Math.min(jsonResults.events.items.length, 3); ++i) {
					self.showRelatedEvent($relatedEventsContainer, jsonResults.events.items[i]);
				}
			}
		}

		self.showErrorMessageInDetails = function () {
			self.$detailsContainer.empty();
			var errorMessageHtml = '<div class="error"> Oops, something went wrong !</div>';
			self.$detailsContainer.append($(errorMessageHtml));
		}
	};

	return Widget;
});
