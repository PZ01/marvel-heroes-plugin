define([
	"module", "jquery", "jquery_md5"
], function(module, $, md5plugin) {
	'use strict';
	var api = {};

	var config = module.config();

	var isAbsoluteUrl = function(uri) {
		var regex = new RegExp('^(?:[a-z]+:)?//', 'i');
		return regex.test(uri);
	}

	api.query = function(resource, data) {
		var timestamp = $.now();

		return $.ajax({
			dataType: "json",

			url: isAbsoluteUrl(resource) ? resource : config.API_BASE_ENDPOINT + resource,

			data: $.extend({
				apikey: config.PUBLIC_KEY,
				hash: $.md5(timestamp + config.PRIVATE_KEY + config.PUBLIC_KEY),
				ts: timestamp
			}, data),
		});
	}

	api.getCharacters = function(nameStartsWith) {
		return api.query('/v1/public/characters', {
			nameStartsWith: nameStartsWith
		});
	}
	
	api.getComics = function(titleStartsWith) {
		return api.query('/v1/public/comics', {
			titleStartsWith: titleStartsWith
		});
	}

	api.getEvents = function(nameStartsWith) {
		return api.query('/v1/public/events', {
			nameStartsWith: nameStartsWith
		});
	}

	return api;
});
